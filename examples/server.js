const app = require('express')();
const healthCheckMiddlewareGenerator = require('../index');
const extraTestFunction = function() {
  return new Promise((resolve, reject) => {
    checkDB()
      .then(data => {
        if (data.connection === true) {
          resolve({ databaseConnection: true })
        } else {
          let error = new Error("Database is not connected")
          error.data = { databaseConnection: data.connection }
          error.statusCode = 501;
          error.status = 'warning'
          reject(error)
        }
      })
  })
}

function checkDB() {
  return new Promise((resolve) => {
    let data = { connection: (Math.random() < 0.5) }
    resolve(data)
  })
}

const healthCheckMiddleware = healthCheckMiddlewareGenerator({ testFunction: extraTestFunction });

app.use('/health', healthCheckMiddleware);

app.listen(3000, function() {
  console.log('listening on 3000. Try hitting localhost:3000/health')
})
