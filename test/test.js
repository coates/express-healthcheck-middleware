const SpecReporter = require('jasmine-spec-reporter').SpecReporter;

jasmine.getEnv().clearReporters(); // remove default reporter logs
jasmine.getEnv().addReporter(new SpecReporter({ // add jasmine-spec-reporter
  spec: {
    displayPending: true
  }
}));



describe("healthcheck", () => {
  let middlewareGenerator;
  beforeEach(() => {
    process.env.VERSION = '1';
    middlewareGenerator = require('../index');
  })
  it('it should return a function', () => {
    expect(middlewareGenerator).toEqual(jasmine.any(Function))
  });
  describe('arguments', () => {

    it('should accept no arguments', () => {
      let healthCheckMiddleware = middlewareGenerator();
      expect(healthCheckMiddleware).toEqual(jasmine.any(Function))
    })
    describe('should accept 1 argument', () => {
      it('should throw an error if the options.testFunction is not a function', () => {
        expect(function() { middlewareGenerator({ testFunction: 'test' }) }).toThrow()
      })
      it('should throw an error if the options is not a object', () => {
        let testFunc = () => {
          return { msg: 'test' }
        };
        expect(function() {middlewareGenerator(testFunc)}).toThrow(new Error('Options must be an object'));
      })
    })
  })
  describe('generated middleware', () => {
    let healthCheckMiddleware, res;
    beforeEach(() => {
      res = {};
      res.status = (code) => {
        return res
      };
      res.json = (response) => {
        return res
      };
      spyOn(process, 'uptime').and.returnValue('uptime');

    })
    it('should return an object with a version, uptime and status', () => {
      healthCheckMiddleware = middlewareGenerator();
      res.status = (code) => {
        expect(code).toEqual(200);
        return res;
      }

      res.json = (json) => {
        expect(json.version).toEqual('1');
        expect(json.uptime).toEqual('uptime');
        expect(json.status).toEqual('ok');
      }
      healthCheckMiddleware({}, res);
    })
    describe('should accept passing a custom async test function', () => {
      let msg = "This passed from a promise";
      it('should return the resolved data object', done => {
        let testFunction = function() {
          return new Promise((resolve, reject) => {
            setTimeout(function() {
              resolve({ msg: msg })
            }, 100);
          })
        }
        healthCheckMiddleware = middlewareGenerator({ testFunction: testFunction });
        res.json = (json) => {
          expect(json.data.msg).toEqual(msg);
          done()
        }

        healthCheckMiddleware({}, res)
      })
      it('should return an unhealthy status if an error is thrown', done => {
        let testFunction = function() {
          return new Promise((resolve, reject) => {
            setTimeout(function() {
              reject(new Error(msg))
            }, 100);
          })
        }
        healthCheckMiddleware = middlewareGenerator({ testFunction: testFunction });
        res.json = (json) => {
          expect(json.message).toEqual(msg);
          expect(json.statusCode).toEqual(500);
          expect(json.status).toEqual('error');
          done()
        }

        healthCheckMiddleware({}, res)
      })
      it('should return an unhealthy status if an error is thrown and allow overwriting the defaults', done => {
        let testFunction = function() {
          return new Promise((resolve, reject) => {
            setTimeout(function() {
              let error = new Error(msg);
              error.statusCode = 501;
              error.status = 'critical';
              reject(error)
            }, 100);
          })
        }
        healthCheckMiddleware = middlewareGenerator({ testFunction: testFunction });
        res.json = (json) => {
          expect(json.message).toEqual(msg);
          expect(json.statusCode).toEqual(501);
          expect(json.status).toEqual('critical');
          done()
        }

        healthCheckMiddleware({}, res)
      })
    })

    describe('should accept a sync test function', () => {
      let msg = "This passed from a sync func";
      it('should return the returned data object', () => {

        let testFunction = () => {
          return { msg: msg }
        }
        healthCheckMiddleware = middlewareGenerator({ testFunction: testFunction });
        res.json = (json) => {
          expect(json.data.msg).toEqual(msg);
        }
        healthCheckMiddleware({}, res)
      })
      it('should return the allow just strings to be returned and wrap them in a data obj', () => {
        let testFunction = () => {
          return msg;
        }
        healthCheckMiddleware = middlewareGenerator({ testFunction: testFunction });
        res.json = (json) => {
          expect(json.data.info).toEqual(msg);
        }
        healthCheckMiddleware({}, res)
      })
      it('should return the returned data object and allow overwriting', () => {
        let testFunction = () => {
          return { statusCode:201, msg: msg }
        }
        healthCheckMiddleware = middlewareGenerator({ testFunction: testFunction });
        res.json = (json) => {
          expect(json.statusCode).toEqual(201);
          expect(json.data.msg).toEqual(msg);
        }
        healthCheckMiddleware({}, res)
      })
      it('should allow overwriting the default statusCode and status message', () => {
        let msg = "This passed from a sync func";
        let testFunction = () => {
          return { statusCode: 501, status: 'warning', uptime: 'fake' }
        }
        healthCheckMiddleware = middlewareGenerator({ testFunction: testFunction });
        res.json = (json) => {
          expect(json.status).toEqual('warning');
        }
        res.status = (code) => {
          expect(code).toEqual(501);
          return res;
        }
        healthCheckMiddleware({}, res)
      })

      it('should return a unhealthy status if an error is thrown', () => {
        let testFunction = () => {
          throw new Error('An error occurred');
        }
        res.status = (code) => {
          expect(code).toEqual(500);
          return res;
        }
        res.json = (response) => {
          expect(response.status).toEqual('error');
          expect(response.message).toEqual('An error occurred');
        }
        healthCheckMiddleware = middlewareGenerator({ testFunction: testFunction });
        healthCheckMiddleware({}, res)
      })
      it('should return a unhealthy status if an error is thrown and allow overwriting the defaults', () => {
        let testFunction = () => {
          let error = new Error('An error occurred');
          error.status = 'critical';
          error.statusCode = 504;
          throw error;
        }
        res.status = (code) => {
          expect(code).toEqual(504);
          return res;
        }
        res.json = (response) => {
          expect(response.status).toEqual('critical');
          expect(response.message).toEqual('An error occurred');
        }
        healthCheckMiddleware = middlewareGenerator({ testFunction: testFunction });
        healthCheckMiddleware({}, res)
      })
    })
  })
})