# Pull Request Process

- Branch from `master`
- Follow the git flow convention of 
  - `feature/` - adding new functionality that consumers of the module will see 
  - `bugix/` - Fixing any bugs
  - `upkeep/` - Fixing/Updating any part that consumers _don't_ see. EG unit tests.

## Before submitting a Request

- Add/update tests
- Ensure you run `npm test` and it passes
- Update README.md with any new information
- Cleanup any code (Linter once implemented)
- Make a Pull Request (Merge Request) to `master`