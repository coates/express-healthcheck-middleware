const _ = require('lodash');
const version = process.env.VERSION || null;

module.exports = function(options = {}) {
  if (typeof options !== 'object') {
    throw new Error('Options must be an object');
  }
  if (options.testFunction !== undefined && typeof options.testFunction !== 'function') {
    throw new Error("options.testFunction must be a function");
  }

  return function(req, res) {
    checkHealthStatus(options.testFunction)
      .then(extraInfo => {
        return generateHealthInfo(extraInfo);
      })
      .catch(error => {
        return generateHealthInfo(error)
      })
      .then(healthObj => {
        res.status(healthObj.statusCode).json(healthObj);
      })
  }

  function generateHealthInfo(extraInfo) {
    let healthObj = {};
    let data;

    if (typeof extraInfo != 'object') {
      extraInfo = { info: extraInfo }
    }

    if (extraInfo instanceof Error) {
      data = extraInfo.data;
      let statusCode = extraInfo.statusCode || 500;
      let status = extraInfo.status || 'error';
      healthObj.message = extraInfo.message;
      extraInfo = { status, statusCode }
    } else {
      data = _.omit(extraInfo, ['status', 'statusCode', 'message']);
    }
    if (data === undefined) data = {};

    healthObj.statusCode = extraInfo.statusCode || 200;
    healthObj.status = extraInfo.status || 'ok';
    healthObj.uptime = process.uptime();
    healthObj.version = version;
    healthObj.data = data;
    return healthObj;
  }

  function checkHealthStatus(extraTests) {
    return new Promise((resolve, reject) => {
      if (extraTests === undefined) resolve({});
      let result = extraTests();
      if (result instanceof Promise === false) {
        result = Promise.resolve(result); // Wrap it in a promise.
      }
      result
        .then(resolve)
        .catch(reject)
    })
  }
}
