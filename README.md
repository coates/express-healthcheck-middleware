# Express Healthcheck Middleware

## What it is

This module can be plugged into express applications to expose a health check endpoint for external services.

## How to use it

This module will export a middleware generator. The generator can be used by itself or by passing in an optional argument.
The generator returns an express Middleware function with parameters `req` and `res`

By default the generator will export a middleware that returns the following json object when triggered

|  Paramter  |              Description               |                    Default values                    |
|------------|----------------------------------------|------------------------------------------------------|
| version    | The current version of the application | `process.env.VERSION` or `null` if that is undefined |
| status     | The text health status                 | 'ok' or 'error'                                      |
| statusCode | The HTTP status code                   | `200` or `500`                                       |
| uptime     | The uptime of the node application     | float eg `50.301`                                      |

Extra Optional Arguments

| Paramter |          Description          |     Example values    |
|----------|-------------------------------|-----------------------|
| data     | Only checked on Error objects | { connection: false } |


The `status`, `statusCode` can be overriden if a test function returns the values.

### Arguments

The options argument contains the following parameters

`testFunction`

- A function returning a promise (or just a regular synchronous function) that can run any extra tests.
- This should resolve or reject with a Object containing any extra information to display.
- `status`, `statusCode` attributes can be returned to override the defaults noted above
- If an error is thrown it will be caught and return the `Error.message` + a `500` `statusCode` obj along with anything put into `Error.data`

If a non object is returned from this function then the middleware will wrap it in the in a `data` object with the key `info`

EG `return 'this is a message'` becomes

```
{
  data: {
    info: 'this is a message'
  }
}
```

## Examples

Basic use

```javascript 
const healthCheckMiddlewareGenerator = require('express-healthcheck-middleware');
const healthCheckMiddleware = healthCheckMiddlewareGenerator();

app.use('/health', healthCheckMiddleware);
```

This example will also run an extra test case of checking if the Database connection is alive.
(Assume the function `checkDB` resolves to a object with `connection` of `true` or `false`)

```javascript
const healthCheckMiddlewareGenerator = require('express-healthcheck-middleware');
const extraTestFunction = function() {
  return new Promise((resolve, reject) => {
    checkDB()
      .then(data => {
        if (data.connection === true) {
          resolve({ databaseConnection: true })
        } else {
          let error = new Error("Database is not connected")
          error.data = { databaseConnection: data.connection }
          error.statusCode = 501;
          error.status = 'warning';
          reject(error);
        }
      })
  })
}

const healthCheckMiddleware = healthCheckMiddlewareGenerator({ testFunction: extraTestFunction });

app.use('/health', healthCheckMiddleware);
```


## Developing

Running this command will startup a docker container mounting your source into `/home/`
You can then `cd` into `/home` and dev as normal. Care should be taken for root files when creating them within the container.

`docker run -it -v $PWD/:/home/ --rm node bash`

### NPM commands

- `npm install` to install the application for development
- `npm test` to run all tests
- `npm run example` to load up a server with an example use (see `examples/server.js`)

# TODO

- Add function to manipulate/format final json data.
- Add more error handling